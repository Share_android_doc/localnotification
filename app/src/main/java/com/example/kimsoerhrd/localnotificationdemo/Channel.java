package com.example.kimsoerhrd.localnotificationdemo;

public class Channel {

    String id;
    String name;
    int priority;
    String dec;

    public Channel() {
    }

    public Channel(String id, String name, int priority, String dec) {
        this.id = id;
        this.name = name;
        this.priority = priority;
        this.dec = dec;
    }

    public void setDec(String dec) {
        this.dec = dec;
    }

    public String getDec() {
        return dec;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getPriority() {
        return priority;
    }

    public void setPriority(int priority) {
        this.priority = priority;
    }
}
