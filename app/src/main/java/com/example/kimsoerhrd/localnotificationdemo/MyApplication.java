package com.example.kimsoerhrd.localnotificationdemo;

import android.annotation.TargetApi;
import android.app.Application;
import android.app.Notification;
import android.app.NotificationManager;
import android.content.Context;
import android.graphics.Color;
import android.os.Build;



public class MyApplication extends Application {
    @Override
    public void onCreate() {
        super.onCreate();
        Channel channel=new Channel("Channel_ID","ChannelName", NotificationManager.IMPORTANCE_HIGH, "Android_Channel");
        createNotificationChannel(this,channel,null);
    }

    @TargetApi(Build.VERSION_CODES.O)
    public static<T extends Channel> void createNotificationChannel(Context context, T channel, String...groupIds){
        if (Build.VERSION.SDK_INT  >= Build.VERSION_CODES.O){
            android.app.NotificationChannel mChanel = new android.app.NotificationChannel(channel.getId(),channel.getName(), channel.getPriority());
            mChanel.setDescription(channel.getDec());
            mChanel.enableLights(true);
            mChanel.setLightColor(Color.RED);
            mChanel.enableVibration(true);
            mChanel.setVibrationPattern(new long[]{100l, 200l, 300l, 400l,500l, 400l, 300l, 200l, 400l});
            NotificationManager manager = (NotificationManager) context.getSystemService(Context.NOTIFICATION_SERVICE);
            manager.createNotificationChannel(mChanel);
        }

    }


}
