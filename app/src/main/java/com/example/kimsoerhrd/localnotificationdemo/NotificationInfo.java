package com.example.kimsoerhrd.localnotificationdemo;

import android.os.Parcel;
import android.os.Parcelable;

public class NotificationInfo implements Parcelable {

    String contentTitle;
    String contentText;
    int smallIcon;

    public NotificationInfo() {
    }

    public NotificationInfo(String contentTitle, String contentText, int smallIcon) {
        this.contentTitle = contentTitle;
        this.contentText = contentText;
        this.smallIcon = smallIcon;
    }

    protected NotificationInfo(Parcel in) {
        contentTitle = in.readString();
        contentText = in.readString();
        smallIcon = in.readInt();
    }

    public static final Creator<NotificationInfo> CREATOR = new Creator<NotificationInfo>() {
        @Override
        public NotificationInfo createFromParcel(Parcel in) {
            return new NotificationInfo(in);
        }

        @Override
        public NotificationInfo[] newArray(int size) {
            return new NotificationInfo[size];
        }
    };

    public String getContentTitle() {
        return contentTitle;
    }

    public void setContentTitle(String contentTitle) {
        this.contentTitle = contentTitle;
    }

    public String getContentText() {
        return contentText;
    }

    public void setContentText(String contentText) {
        this.contentText = contentText;
    }

    public int getSmallIcon() {
        return smallIcon;
    }

    public void setSmallIcon(int smallIcon) {
        this.smallIcon = smallIcon;
    }

    @Override
    public String toString() {
        return "NotificationInfo{" +
                "contentTitle='" + contentTitle + '\'' +
                ", contentText='" + contentText + '\'' +
                ", smallIcon=" + smallIcon +
                '}';
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(contentTitle);
        dest.writeString(contentText);
        dest.writeInt(smallIcon);
    }
}
