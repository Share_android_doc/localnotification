package com.example.kimsoerhrd.localnotificationdemo;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.widget.ImageView;
import android.widget.TextView;

public class NotificationActivity extends AppCompatActivity {

    TextView textView, tvText;

    ImageView imageView;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.notification_layout);

        textView = findViewById(R.id.tvTitle);
        tvText = findViewById(R.id.tvText);
        imageView = findViewById(R.id.imageView);

        Intent intent = getIntent();
        NotificationInfo info =intent.getParcelableExtra("data");
        textView.setText(info.getContentTitle().toString());
        tvText.setText(info.getContentText().toString());
        imageView.setImageResource(info.getSmallIcon());








    }
}
