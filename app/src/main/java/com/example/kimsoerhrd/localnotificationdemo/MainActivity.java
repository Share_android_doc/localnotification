package com.example.kimsoerhrd.localnotificationdemo;

import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.graphics.BitmapFactory;
import android.media.MediaPlayer;
import android.media.RingtoneManager;
import android.net.Uri;
import android.support.v4.app.NotificationCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;


public class MainActivity extends AppCompatActivity {

    Button button;
    Channel channel;
    NotificationInfo info;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        info = new NotificationInfo("New App Undate",
                "You can update new app for new functionality",
                R.drawable.ic_launcher_background);

        button = findViewById(R.id.btnNotify);
        channel = new Channel();
        channel.setId("Channel_ID");
        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(MainActivity.this, NotificationActivity.class);
                Bundle bundle = new Bundle();
                bundle.putParcelable("data", info);
                intent.putExtras(bundle);
                PendingIntent pendingIntent = PendingIntent.getActivity(MainActivity.this, 1, intent,
                        PendingIntent.FLAG_CANCEL_CURRENT);


                NotificationCompat.Builder builder = new NotificationCompat.Builder(MainActivity.this, channel.getId());
                builder
                        .setContentTitle(info.getContentTitle())
                        .setLargeIcon(BitmapFactory.decodeResource(getResources(), R.drawable.images))
                        .setSmallIcon(info.getSmallIcon())
                        .setContentText(info.getContentText())
                        .setSound(Uri.parse("android.resource://"+getApplicationContext()+"/"+R.raw.sounds))
                        //.setPriority(NotificationManager.IMPORTANCE_LOW)
                       // .setDefaults(Notification.DEFAULT_SOUND)
                        .setContentIntent(pendingIntent)
                        .setTicker("Ticker");

                NotificationManager notificationManager = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
                int notificationId = (int) (System.currentTimeMillis()/1000);
                notificationManager.notify(notificationId, builder.build());
            }
        });
    }


}
